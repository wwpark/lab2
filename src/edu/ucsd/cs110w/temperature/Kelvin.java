/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (wpark): write class javadoc
 *
 * @author wpark
 *
 */
public class Kelvin extends Temperature{

	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO: Complete this method
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float) (conv - 273.2);
		converted = new Celsius(conv);
		return converted;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float) ((conv-273.2)*1.8+32);
		converted = new Fahrenheit(conv);
		return converted;
	}
	@Override
	public Temperature toKelvin()
	{
		return this;
	}

}
