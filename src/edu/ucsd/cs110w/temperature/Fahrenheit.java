/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author wpark
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float)((conv-32)/1.8);
		converted = new Celsius(conv);
		return converted;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float) ((conv-32)/1.8+273.2);
		converted = new Kelvin(conv);
		return converted;
	}
}
