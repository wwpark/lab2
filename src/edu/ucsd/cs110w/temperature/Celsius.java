/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author wpark
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	} 
	public String toString()
	{
		return Float.toString(getValue());
		// TODO: Complete this method return "";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float)(1.8*conv+32);
		converted = new Fahrenheit(conv);
		return converted;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Temperature converted;
		float conv;
		conv = getValue();
		conv = (float) (conv+273.2);
		converted = new Kelvin(conv);
		return converted;
	}
}
